package de.fabianmittmann.knightgame.logic;

import de.fabianmittmann.knightgame.domain.Knight;
import de.fabianmittmann.knightgame.domain.KnightCircle;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * Provides the method to determine the winner of the knight game.
 *
 * @author Fabian Mittmann
 */
class WinnerSelector {

    private KnightCircle knightCircle;
    private LinkedList<Knight> deadKnightsTemp;

    WinnerSelector(KnightCircle knightCircle) {
        this.knightCircle = knightCircle;
        deadKnightsTemp = new LinkedList<>();
    }

    /**
     * Determines the winner out of a given {@link KnightCircle}.
     *
     * @return  the winner of the game out of the specified {@link KnightCircle}
     * @see     #sleep(int)
     * @see     #processRound()
     * @see     #removeDeadKnightsFromCircle()
     */
    Knight getWinner() {
        while (knightCircle.size() > 1) {
            // wait few seconds to make output more readable
            sleep(3);
            // fight!
            processRound();
            // remove dead knights from circle and reset list
            removeDeadKnightsFromCircle();
        }
        return knightCircle.getFirst();
    }

    /**
     * Executes a complete round in the specified {@link KnightCircle}.
     *
     * @see     #processAttack(Knight)
     */
    private void processRound() {
        System.out.println("\nNew round - " + knightCircle.size() + " Knights still standing: " + knightCircle);
        knightCircle.forEach(currentKnight -> {
            if (currentKnight.getHealthPoints() > 0) {
                processAttack(currentKnight);
            }
        });
    }

    /**
     * Executes an attack from a {@link Knight} in a {@link KnightCircle}.
     *
     * @param   attacker a {@link Knight} attacking
     * @see     #findTargetKnight(Knight)
     */
    private void processAttack(Knight attacker) {
        // fight!
        Knight target = findTargetKnight(attacker);
        if (target.getHealthPoints() > 0) {
            attacker.doDamageTo(target);
            if (target.getHealthPoints() == 0) {
                deadKnightsTemp.add(target);
            }
        }
    }

    /**
     * Looks for the target of a given {@link Knight} in the specified {@link KnightCircle}.
     * @param   attacker
     * @return  the knight the given attacker would hit
     */
    private Knight findTargetKnight(Knight attacker) {
        int currentIndex = knightCircle.indexOf(attacker);
        int rangeOfAttack = attacker.getWeapon().getRangePoints();
        // degrease range if its too big to hit an target
        if (rangeOfAttack > knightCircle.size() - 1) {
            rangeOfAttack = knightCircle.size() - 1;
        }
        // calculate enemy index
        if (currentIndex + rangeOfAttack > knightCircle.size() - 1) {
            int elementsAfterCurrent = knightCircle.size() - currentIndex - 1;
            int indexOfEnemy = rangeOfAttack - elementsAfterCurrent -1;
            return knightCircle.get(indexOfEnemy);
        } else {
            return knightCircle.get(currentIndex + rangeOfAttack);
        }
    }

    /**
     * Removes all knights with state dead from the specified {@link KnightCircle}
     * and clears the temporary list containing the knights died in the last round.
     */
    private void removeDeadKnightsFromCircle() {
        if (!deadKnightsTemp.isEmpty()) {
            knightCircle.removeAll(deadKnightsTemp);
            deadKnightsTemp.clear();
        }
    }

    /**
     * Util method to wait the specified amount of seconds.
     * @param   seconds an amount of seconds to wait
     */
    private void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
