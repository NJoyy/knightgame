package de.fabianmittmann.knightgame.logic;

import de.fabianmittmann.knightgame.domain.Knight;
import de.fabianmittmann.knightgame.domain.KnightCircle;

/**
 * Class with static method to start the game.
 *
 * @author Fabian Mittmann
 */
public class GameStarter {

    /**
     * Method to start the kight game.
     *
     * @param   knightCircle the {@link KnightCircle} the game should be played at.
     */
    public static void startGame(KnightCircle knightCircle) {
        System.out.println("Game begins... " + knightCircle.size() + " Knights are ready to fight...");
        WinnerSelector winnerSelector = new WinnerSelector(knightCircle);
        Knight winner = winnerSelector.getWinner();
        System.out.println("\nAnd the Winner is " + winner + "!");
    }

}
