package de.fabianmittmann.knightgame;

import de.fabianmittmann.knightgame.domain.Knight;
import de.fabianmittmann.knightgame.domain.KnightCircle;
import de.fabianmittmann.knightgame.domain.Weapon;
import de.fabianmittmann.knightgame.logic.GameStarter;

import java.util.LinkedList;

/**
 * Main class to start the application.
 *
 * @author Fabian Mittmann
 */
public class App {

    /**
     * Main method to start the application.
     *
     * @param args
     */
    public static void main( String[] args ) {
        GameStarter.startGame(bootstrapKnightCircle());
    }

    /**
     * Generate some data to play the game with.
     *
     * @return a {@link KnightCircle} as sample data
     */
    private static KnightCircle bootstrapKnightCircle() {
        LinkedList<Knight> bootstrapKnights = new LinkedList<>();

        bootstrapKnights.add(new Knight(0, Weapon.SWORD));
        bootstrapKnights.add(new Knight(1, Weapon.BOW));
        bootstrapKnights.add(new Knight(2, Weapon.SPEAR));
        bootstrapKnights.add(new Knight(3, Weapon.SWORD));
        bootstrapKnights.add(new Knight(4, Weapon.SPEAR, true));
        bootstrapKnights.add(new Knight(10, Weapon.BOW, true));

        return new KnightCircle(bootstrapKnights);
    }

}
