package de.fabianmittmann.knightgame.domain;

import java.util.LinkedList;

/**
 * An extended {@link LinkedList} to represent a circle of {@link Knight}.
 *
 * @author Fabian Mittmann
 */
public class KnightCircle extends LinkedList<Knight> {

    public KnightCircle(LinkedList<Knight> knightsInBattle) {
        if (knightsInBattle.isEmpty()) {
            throw new IllegalStateException("ERROR: circle can not exist with zero Knights!");
        }
        this.addAll(knightsInBattle);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        this.forEach(knight -> sb.append("\t").append(knight).append("\n"));
        sb.append("}");

        return sb.toString();
    }

}
