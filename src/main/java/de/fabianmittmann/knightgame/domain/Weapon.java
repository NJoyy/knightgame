package de.fabianmittmann.knightgame.domain;

/**
 * Domain class representing a weapon of a {@link Knight}.
 * Has a damage ratio and a range for it.
 *
 * @author Fabian Mittmann
 */
public enum Weapon {

    BOW(1, 3),
    SPEAR(2,2),
    SWORD(3, 1);

    private int damage;
    private int rangePoints;

    Weapon(int damage, int rangePoints) {
        this.damage = damage;
        this.rangePoints = rangePoints;
    }

    public int getDamage() {
        return damage;
    }

    public int getRangePoints() {
        return rangePoints;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                this.name() + " with " +
                "damage=" + damage +
                ", rangePoints=" + rangePoints +
                '}';
    }

}
