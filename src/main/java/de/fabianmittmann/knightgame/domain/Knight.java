package de.fabianmittmann.knightgame.domain;

import java.util.Objects;

/**
 * Domain class for a knight fighting in the game.
 * Every knight has health, health-protecting armor and maybe
 * an optional elixir to regenerating health if it goes down to 0.
 * Knights can fight against each other.
 *
 * @author Fabian Mittmann
 */
public class Knight {

    private static final int INITIAL_HEALTH = 10;

    private int healthPoints;
    private int armorPoints;
    private boolean elixir;
    private boolean alive;

    private Weapon weapon;

    public Knight(int armorPoints, Weapon weapon, boolean elixir) {
        this.alive = true;
        this.healthPoints = INITIAL_HEALTH;
        this.armorPoints = armorPoints;
        this.elixir = elixir;
        this.weapon = Objects.requireNonNull(weapon);
    }

    public Knight(int armorPoints, Weapon weapon) {
        this.alive = true;
        this.healthPoints = INITIAL_HEALTH;
        this.elixir = false;
        this.armorPoints = armorPoints;
        this.weapon = Objects.requireNonNull(weapon);
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public int getArmorPoints() {
        return armorPoints;
    }

    public boolean hasElixir() {
        return elixir;
    }

    public void addElixir() {
        this.elixir = true;
    }

    public void removeElixir() {
        this.elixir = false;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return "Knight{" +
                "healthPoints=" + healthPoints +
                ", armorPoints=" + armorPoints +
                ", elixir=" + elixir +
                ", weapon=" + weapon +
                " , alive=" + alive +
                '}';
    }

    /**
     * Reduces the health or points of a knight by a specified number of points.
     * If the health points will get to 0 the knight will get marked as not alive anymore.
     * @param damagePoints the amount of points which will decrease the health or armor points of a knight
     */
    public void reduceHealth(int damagePoints) {
        if (this.armorPoints == 0) {
            this.healthPoints -= damagePoints;
        } else {
            int damagedArmorPoints = this.armorPoints - damagePoints;
            if (damagedArmorPoints <= 0) {
                this.armorPoints = 0;
                this.healthPoints += damagedArmorPoints;
            } else {
                this.armorPoints = damagedArmorPoints;
            }
        }
        checkIfAlive();
    }

    /**
     * Reduces the health of a specified knight by the damage value of the weapon of this knight.
     * @param enemy a specified knight this knight will damage
     */
    public void doDamageTo(Knight enemy) {
        if (enemy.getHealthPoints() > 0) {
            enemy.reduceHealth(this.weapon.getDamage());
            if (enemy.getHealthPoints() == 0) {
                System.out.println("\n" + enemy + " killed by " + this);
            }
        }
    }

    /**
     * Checks the health of this knight to determine if the health decreased to 0.
     * If this knight has an elixir, health will regenerate completely.
     */
    private void checkIfAlive() {
        if (this.healthPoints <= 0) {
            if (this.hasElixir()) {
                this.removeElixir();
                this.healthPoints = INITIAL_HEALTH;
                System.out.println("\nElixir used by " + this + ", health regenerated completely.");
            } else {
                this.healthPoints = 0;
                this.alive = false;
            }
        }
    }

}
