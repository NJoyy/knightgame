package de.fabianmittmann.knightgame.logic;

import de.fabianmittmann.knightgame.domain.Knight;
import de.fabianmittmann.knightgame.domain.KnightCircle;
import de.fabianmittmann.knightgame.domain.Weapon;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for {@link WinnerSelector}.
 *
 * @author Fabian Mittmann
 */
class WinnerSelectorTest {

    private WinnerSelector winnerSelector;

    /**
     * Test for getting the winner of the match.
     */
    @Test
    void getWinnerTest() {
        winnerSelector = new WinnerSelector(bootstrapKnightCircle());

        Knight winner = winnerSelector.getWinner();

        assertEquals(7, winner.getHealthPoints());
        assertEquals(Weapon.SWORD, winner.getWeapon());
    }

    /**
     * Generating some test data.
     */
    private KnightCircle bootstrapKnightCircle() {
        LinkedList<Knight> bootstrapKnights = new LinkedList<>();

        bootstrapKnights.add(new Knight(0, Weapon.SWORD));
        bootstrapKnights.add(new Knight(0, Weapon.BOW));

        return new KnightCircle(bootstrapKnights);
    }

}
