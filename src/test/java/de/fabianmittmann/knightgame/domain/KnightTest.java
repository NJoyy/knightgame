package de.fabianmittmann.knightgame.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Tests for class {@link Knight}.
 *
 * @author Fabian Mittmann
 */
class KnightTest {

    @Test
    void reduceHealthTest() {
        Knight knight = new Knight(0, Weapon.SWORD);
        knight.reduceHealth(5);
        assertEquals(5, knight.getHealthPoints());
    }

    /**
     * Test with damage bigger than initial health.
     */
    @Test
    void reduceHealthWithBigDamageTest() {
        Knight knight = new Knight(0, Weapon.SWORD);
        knight.reduceHealth(1000);
        assertEquals(0, knight.getArmorPoints());
    }

    /**
     * Test with armor and damage lower than armor points.
     */
    @Test
    void reduceHealthWithArmorTest() {
        Knight knight = new Knight(5, Weapon.SWORD);
        knight.reduceHealth(3);
        assertEquals(2, knight.getArmorPoints());
        assertEquals(10, knight.getHealthPoints());
    }

    /**
     * Test with armor and damage bigger than armor points.
     */
    @Test
    void reduceHealthWithBrokenArmorTest() {
        Knight knight = new Knight(5, Weapon.SWORD);
        knight.reduceHealth(8);
        assertEquals(0, knight.getArmorPoints());
        assertEquals(7, knight.getHealthPoints());
    }

    /**
     * Test with elixir healing up health completely.
     */
    @Test
    void reduceHealthWithElixirTest() {
        Knight knight = new Knight(0, Weapon.SWORD, true);
        knight.reduceHealth(12);
        assertFalse(knight.hasElixir());
        assertEquals(0, knight.getArmorPoints());
        assertEquals(10, knight.getHealthPoints());
    }

    /**
     * Test with sword damage.
     */
    @Test
    void doDamageToWithSwordTest() {
        Knight fighter = new Knight(0, Weapon.SWORD);
        Knight enemy = new Knight(0, Weapon.SWORD);
        // sword = 3 damage points
        fighter.doDamageTo(enemy);
        assertEquals(7, enemy.getHealthPoints());
    }

    /**
     * Test with spear damage.
     */
    @Test
    void doDamageToWithSpearTest() {
        Knight fighter = new Knight(0, Weapon.SPEAR);
        Knight enemy = new Knight(0, Weapon.SPEAR);
        // spear = 2 damage points
        fighter.doDamageTo(enemy);
        assertEquals(8, enemy.getHealthPoints());
    }

    /**
     * Test with bow damage.
     */
    @Test
    void doDamageToWithBowTest() {
        Knight fighter = new Knight(0, Weapon.BOW);
        Knight enemy = new Knight(0, Weapon.BOW);
        // bow = 1 damage point
        fighter.doDamageTo(enemy);
        assertEquals(9, enemy.getHealthPoints());
    }

    /**
     * Test to kill a enemy.
     */
    @Test
    void doDamageToWithKill() {
        Knight fighter = new Knight(0, Weapon.SWORD);
        Knight enemy = new Knight(0, Weapon.BOW);
        for (int i = 0; i <= 5; i++) {
            fighter.doDamageTo(enemy);
        }
        assertEquals(0, enemy.getHealthPoints());
    }

}